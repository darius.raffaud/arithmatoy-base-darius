#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "utils.h"

int VERBOSE = 0;

const char *get_all_digits() { return "0123456789abcdefghijklmnopqrstuvwxyz"; }
const size_t ALL_DIGIT_COUNT = 36;

void arithmatoy_free(char *number) { free(number); }

char *arithmatoy_add(unsigned int base, const char *lhs, const char *rhs) {
  if (VERBOSE) {
    fprintf(stderr, "add: entering function\n");
  }

  // Fill the function, the goal is to compute lhs + rhs
  // You should allocate a new char* large enough to store the result as a
  // string Implement the algorithm Return the result

  size_t lhs_length = strlen(lhs);
  size_t rhs_length = strlen(rhs);
  size_t result_length = (lhs_length > rhs_length ? lhs_length : rhs_length) + 1;
  char *result = malloc(result_length + 1);
  if (!result) {
    fprintf(stderr, "Memory allocation failed\n");
    exit(EXIT_FAILURE);
  }
  memset(result, '0', result_length);
  result[result_length] = '\0'; // Null-terminate the string
  
  size_t carry = 0;
  unsigned int lhs_value;
  unsigned int rhs_value;
  unsigned int digit_sum = 0;

  for (size_t i = 0; i < result_length; ++i) {
    lhs_value = (i < lhs_length ? get_digit_value(lhs[lhs_length - i - 1]) : 0);
    rhs_value = (i < rhs_length ? get_digit_value(rhs[rhs_length - i - 1]) : 0);
    
    if (VERBOSE) {
      if (i != result_length - 1) {
        fprintf(stderr, "add: digit %c digit %c carry %zu\n", to_digit(lhs_value), to_digit(rhs_value), carry);
      }
    }

    digit_sum = lhs_value + rhs_value + carry;     
    carry = (i != result_length - 1 && digit_sum >= base) ? 1 : 0;
    digit_sum %= base;

    if (VERBOSE) {
      if (i == result_length - 1 && carry == 1) {
        fprintf(stderr, "add: final carry %zu\n", carry);
      } else {
        fprintf(stderr, "add: result: digit %c carry %zu\n", to_digit(digit_sum), carry);
      }
    }
    result[result_length - i - 1] = to_digit(digit_sum);
  }

  const char *result_final = drop_leading_zeros(result);
  char *final_result = strdup(result_final); // Create a new string with the result
  free(result); // Free the initial result memory
  return final_result;

}

char *arithmatoy_sub(unsigned int base, const char *lhs, const char *rhs) {
  if (VERBOSE) {
    fprintf(stderr, "sub: entering function\n");
  }

  // Fill the function, the goal is to compute lhs - rhs (assuming lhs > rhs)
  // You should allocate a new char* large enough to store the result as a
  // string Implement the algorithm Return the result

  if (VERBOSE) {
    fprintf(stderr, "sub: entering function\n");
  }

  size_t lhs_length = strlen(lhs);
  size_t rhs_length = strlen(rhs);
  size_t result_length = lhs_length;
  
  if (strtol(lhs, NULL, base) < strtol(rhs, NULL, base)) {
    return NULL; // Handle lhs < rhs case
  }

  char *result = malloc(result_length + 1);
  if (!result) {
    fprintf(stderr, "Memory allocation failed\n");
    exit(EXIT_FAILURE);
  }
  memset(result, '0', result_length);
  result[result_length] = '\0'; // Null-terminate the string

  size_t carry = 0;
  unsigned int lhs_value;
  unsigned int rhs_value;
  int digit_sub = 0;

  char *reverse_lhs = strdup(lhs); // Duplicate lhs and reverse
  char *reverse_rhs = strdup(rhs); // Duplicate rhs and reverse
  reverse(reverse_lhs);
  reverse(reverse_rhs);

  for (size_t i = 0; i < result_length; i++) {
    lhs_value = (i < lhs_length ? get_digit_value(reverse_lhs[i]) : 0);
    rhs_value = (i < rhs_length ? get_digit_value(reverse_rhs[i]) : 0);

    if (VERBOSE) {
      fprintf(stderr, "sub: digit %c digit %c carry %zu\n", to_digit(lhs_value), to_digit(rhs_value), carry);     
    }

    digit_sub = lhs_value - rhs_value - carry;
    carry = digit_sub < 0 ? 1 : 0;
    if (carry) {
      digit_sub += base;
    }

    if (VERBOSE) {
      fprintf(stderr, "sub: result: digit %c carry %zu\n", to_digit(digit_sub), carry);
    }
    result[result_length - i - 1] = to_digit(digit_sub);
  }

  free(reverse_lhs); // Free duplicated strings
  free(reverse_rhs);

  const char *result_final = drop_leading_zeros(result);
  char *final_result = strdup(result_final); // Create a new string with the result
  free(result); // Free the initial result memory

  return final_result;
  
}

char *arithmatoy_mul(unsigned int base, const char *lhs, const char *rhs) {
  if (VERBOSE) {
    fprintf(stderr, "mul: entering function\n");
  }

  // Fill the function, the goal is to compute lhs * rhs
  // You should allocate a new char* large enough to store the result as a
  // string Implement the algorithm Return the result

    size_t len_lhs = strlen(lhs);
    size_t len_rhs = strlen(rhs);
    size_t result_len = len_lhs + len_rhs;
    char *result = (char *)calloc(result_len + 1, sizeof(char));
    if (!result) {
        fprintf(stderr, "Memory allocation failed\n");
        exit(EXIT_FAILURE);
    }

    for (size_t i = 0; i < result_len; ++i) {
        result[i] = '0';
    }

    for (size_t i_rhs = len_rhs; i_rhs > 0; --i_rhs) {
        int digit_rhs = get_digit_value(rhs[i_rhs - 1]);
        int carry = 0;

        for (size_t i_lhs = len_lhs; i_lhs > 0; --i_lhs) {
            int digit_lhs = get_digit_value(lhs[i_lhs - 1]);
            int pos = i_lhs + i_rhs - 1;
            int product = digit_lhs * digit_rhs + carry + get_digit_value(result[pos]);
            carry = product / base;
            result[pos] = to_digit(product % base);

            if (VERBOSE) {
                fprintf(stderr, "mul: digit_lhs=%d digit_rhs=%d carry=%d product=%d\n", digit_lhs, digit_rhs, carry, product);
            }
        }

        result[i_rhs - 1] = to_digit(carry);
    }

    // Remove leading zeros
    char *start = result;
    while (*start == '0' && *(start + 1) != '\0') {
        start++;
    }
    char *cleaned_result = strdup(start);
    free(result);
    return cleaned_result;
}

// Here are some utility functions that might be helpful to implement add, sub
// and mul:

unsigned int get_digit_value(char digit) {
  // Convert a digit from get_all_digits() to its integer value
  if (digit >= '0' && digit <= '9') {
    return digit - '0';
  }
  if (digit >= 'a' && digit <= 'z') {
    return 10 + (digit - 'a');
  }
  return -1;
}

char to_digit(unsigned int value) {
  // Convert an integer value to a digit from get_all_digits()
  if (value >= ALL_DIGIT_COUNT) {
    debug_abort("Invalid value for to_digit()");
    return 0;
  }
  return get_all_digits()[value];
}

char *reverse(char *str) {
  // Reverse a string in place, return the pointer for convenience
  // Might be helpful if you fill your char* buffer from left to right
  const size_t length = strlen(str);
  const size_t bound = length / 2;
  for (size_t i = 0; i < bound; ++i) {
    char tmp = str[i];
    const size_t mirror = length - i - 1;
    str[i] = str[mirror];
    str[mirror] = tmp;
  }
  return str;
}

const char *drop_leading_zeros(const char *number) {
  // If the number has leading zeros, return a pointer past these zeros
  // Might be helpful to avoid computing a result with leading zeros
  if (*number == '\0') {
    return number;
  }
  while (*number == '0') {
    ++number;
  }
  if (*number == '\0') {
    --number;
  }
  return number;
}

void debug_abort(const char *debug_msg) {
  // Print a message and exit
  fprintf(stderr, debug_msg);
  exit(EXIT_FAILURE);
}
